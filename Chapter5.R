#Population dynamics and vaccine simulation

#scenario 1
A<-matrix (0,nrow=8, ncol=8)
A
Ff<-c(0,0,0,0.43)
Ff
for (i in 1:length(Ff)){A[1,i] <- Ff[i]}
A
Sf<-c(0.29,0.212,0.024)
for (i in 1:length(Sf)){A[i+1,i]<-Sf[i]}
A
Fm<-c(0,0,0,0.43)
for (i in 1:length(Fm)){A[5,i]<- Fm[i]}
A
Sm<-c(0.29,0.212,0.018)
for (i in 1:length(Sm)){A[i+5,i+4]<-Sm[i]}
A
P<-c(0.64,0.725,0.916,0.918,0.64,0.725,0.902,0.907)
P
for (i in 1:length(P)) {A[i,i]<- P[i]}
A
N<-matrix(c(0,725418,941083,627388,0,725418,666600,235271),ncol=1, byrow=T)
N
weeks<-226
result<-matrix(nrow=nrow(A),ncol=weeks+1)
result[,1]<-N
result
for (i in 1:weeks) result[,i+1]<- A%*%result[,i]
result
tot_pop<-result[,209:225]
tot_popnoegg<-tot_pop[c(-1,-5),]
b<-matrix (0,nrow=8, ncol=8)
b
Ff<-c(0,0,0,0)
Ff
for (i in 1:length(Ff)){b[1,i] <- Ff[i]}
A
Sf<-c(0.29,0.212,0.024)
for (i in 1:length(Sf)){b[i+1,i]<-Sf[i]}
b
Fm<-c(0,0,0,0)
for (i in 1:length(Fm)){b[5,i]<- Fm[i]}
b
Sm<-c(0.29,0.212,0.018)
for (i in 1:length(Sm)){b[i+5,i+4]<-Sm[i]}
b
P<-c(0,0.725,0.916,0.918,0,0.725,0.902,0.907)
P
for (i in 1:length(P)) {b[i,i]<- P[i]}
b


Nsim<-matrix(c(0,337759.1,876809.5,264148,0,337759.1,748494.1,148600.3),ncol=1, byrow=T)
Nsim
weeks<-16
vacc_pop<-matrix(nrow=nrow(b),ncol=weeks+1)
vacc_pop[,1]<-Nsim
vacc_pop
for (i in 1:weeks) vacc_pop[,i+1]<- b%*%vacc_pop[,i]
vacc_pop
vacc_popnoegg<-vacc_pop[c(-1,-5),]
vacc_popnoegg
sumvpne<-colSums(vacc_popnoegg)
sumvpne
sumtpne<-colSums(tot_popnoegg)
sumtpne
round(sumvpne-sumtpne, 0)


#Simulation scenario 2
N14<-matrix(c(0,168879.6,876809.5,264148,0,168879.6,748494.1,148600.3),ncol=1, byrow=T)
N14
weeks<-16
vacc_pop14<-matrix(nrow=nrow(b),ncol=weeks+1)
vacc_pop14[,1]<-N14
vacc_pop14
for (i in 1:weeks) vacc_pop14[,i+1]<- b%*%vacc_pop14[,i]
vacc_pop14
vacc_pop14ne<-vacc_pop14[c(-1,-5),]
vacc_pop14ne
sumvp14ne<-colSums(vacc_pop14ne)
sumvp14ne
sumtpne<-colSums(tot_popnoegg)
sumtpne


#Sensitivity Analisys
lengthy<-dim(y)[1]
vacweek<-16
m<-matrix (0,nrow=8, ncol=8)
tot_pop<-matrix(nrow=nrow(m), ncol=vacweek+1)
tot_popne<-matrix(nrow=nrow(m),ncol=vacweek+1)
ms<-matrix(0,nrow=8,ncol=8)
scenario1<-matrix(nrow=nrow(m),ncol=vacweek+1)
scenario2<-matrix(nrow=nrow(m),ncol=vacweek+1)
HIlevel<-matrix(nrow=vacweek+1,ncol=lengthy)
HIlevel2<-matrix(nrow=vacweek+1,ncol=lengthy)
weekhcHI<-array(data=0,dim=c(1,lengthy))
weeklcHI<-array(data=0,dim=c(1,lengthy))
weekhcHI2<-array(data=0,dim=c(1,lengthy))
weeklcHI2<-array(data=0,dim=c(1,lengthy))
eigen.values <- c(dim=lengthy)
for (i in 1:lengthy) {m[1,1] <- y[i,1]
m[2,1] <- y[i,2]  
m[5,5] <- y[i,1]  
m[6,5] <- y[i,2]  
m[2,2] <- y[i,3]  
m[3,2] <- y[i,4]  
m[6,6] <- y[i,3]  
m[7,6] <- y[i,4]    
m[3,3] <- y[i,5]  
m[4,3] <- y[i,6]  
m[7,7] <- y[i,7]  
m[8,7] <- y[i,8]  
m[4,4] <- y[i,9]  
m[8,8] <- y[i,10] 
m[1,4] <- y[i,11] 
m[5,4] <- y[i,11] 
N<-matrix(c(0,725418,941083,627388,0,725418,666600,235271),ncol=1, byrow=T)
weeks<-225
result<-matrix(nrow=nrow(m),ncol=weeks+1)
result[,1]<-N

#for loop
for (j in 1:weeks) result[,j+1]<- m%*%result[,j]
eigen.values[i]<- Re(eigen(m)$values[1])
tot_pop<-result[,(209:225)]
tot_pop[c(1,5),]<-0

ms[2,2] <- y[i,3]  #chickPfemale
ms[3,2] <- y[i,4]  #ChickSfemale
ms[6,6] <- y[i,3]  #chickPmale
ms[7,6] <- y[i,4]  #ChickSmale  
ms[3,3] <- y[i,5]  #growerfemaleP
ms[4,3] <- y[i,6]  #growerfemaleS
ms[7,7] <- y[i,7]  #growermaleP
ms[8,7] <- y[i,8]  #growermaleS
ms[4,4] <- y[i,9]  #HensP
ms[8,8] <- y[i,10] #RoosterP  

scenario1[,1]<-result[,209]
scenario1[c(1,5),]<-0
for (s in 1:vacweek) scenario1[,s+1]<-ms%*%scenario1[,s]
sumvaccscenario1<-colSums(scenario1)
sumtotpop<-colSums(tot_pop)
HIlevel[,i]<-round(sumvaccscenario1/sumtotpop,3)

scenario2[,1]<-result[,209]
scenario2[c(1,5),]<-0
scenario2[c(2,6),1]<-scenario2[c(2,6),1]/2
for (s in 1:vacweek) scenario2[,s+1]<-ms%*%scenario2[,s]
sumvaccscenario2<-colSums(scenario2)
sumtotpop<-colSums(tot_pop)
HIlevel2[,i]<-round(sumvaccscenario2/sumtotpop,3)
weeksaftervacc<-c(0:16)

hcHI<-subset(weeksaftervacc,HIlevel[,i] < 0.67)                                        
weekhcHI[,i]<-hcHI[1]

lcHI<-subset(weeksaftervacc,HIlevel[,i] < 0.50)                                        
weeklcHI[,i]<-lcHI[1]

hcHI2<-subset(weeksaftervacc,HIlevel2[,i] < 0.67)                   
weekhcHI2[,i]<-hcHI2[1]

lcHI2<-subset(weeksaftervacc,HIlevel2[,i] < 0.50)                   
weeklcHI2[,i]<-lcHI2[1]

}
PweekhcHI<-weekhcHI[,eigen.values>0.98 & eigen.values < 1.02]
PweeklcHI<-weeklcHI[,eigen.values>0.98 & eigen.values < 1.02]
PweekhcHI2<-weekhcHI2[,eigen.values>0.98 & eigen.values < 1.02]
PweeklcHI2<-weeklcHI2[,eigen.values>0.98 & eigen.values < 1.02]
